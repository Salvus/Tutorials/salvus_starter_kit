{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style='background-image: url(\"header.png\") ; padding: 0px ; background-size: cover ; border-radius: 5px ; height: 250px'>\n",
    "    <div style=\"float: right ; margin: 50px ; padding: 20px ; background: rgba(255 , 255 , 255 , 0.7) ; width: 50% ; height: 150px\">\n",
    "        <div style=\"position: relative ; top: 50% ; transform: translatey(-50%)\">\n",
    "            <div style=\"font-size: xx-large ; font-weight: 900 ; color: rgba(0 , 0 , 0 , 0.8) ; line-height: 100%\">Salvus</div>\n",
    "            <div style=\"font-size: large ; padding-top: 20px ; color: rgba(0 , 0 , 0 , 0.5)\">OBS Receivers</div>\n",
    "        </div>\n",
    "    </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The nature of the spectral-element method allows us to place sources and receivers in arbitrary locations, including exactly along element boundaries. This feature is very useful when considering ocean-bottom deployments. In this short tutorial we'll use the classic Foothills model as a proxy for an oceanic setting with strong topography. We'll read in the SEG-Y file defining the model, mesh it up automatically, and then place an OBS line along the coupling interface. As always, we'll start by loading our Python libraries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python libraries.\n",
    "import os\n",
    "import copy\n",
    "import toml\n",
    "import shutil\n",
    "import numpy as np\n",
    "\n",
    "from pathlib import Path\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Salvus.\n",
    "import salvus_mesh             # Salvus meshing toolbox.\n",
    "import salvus_flow.api         # Salvus workflow toolbox.\n",
    "from pyasdf import ASDFDataSet # ASDF library for visualizing seismograms.\n",
    "\n",
    "# Helper routines for meshing SEG-Y files.\n",
    "import segy_mesh_2d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Meshing SEG-Y files\n",
    "\n",
    "To aid Salvus_Mesh_ in the meshing process, we'll explicitly specify the information contained in the SEG-Y headers, and create our own lightweight header to pass the the SEG-Y meshing routines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dx, dy = 15, 10\n",
    "nx, ny = 1668, 1000\n",
    "x0, y0 = 0.0, 2000.0\n",
    "header = segy_mesh_2d.Header2D(x0=x0, y0=y0, nx=nx, ny=ny, dx=dx, dy=dy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll read in the regular-gridded VP SEG-Y file, and compute some proxies for the other elastic parameters we are interested in. In this case we'll use a default Poisson's ratio of $0.25$, and use Gardner's relationship to compute density from VP."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vp = segy_mesh_2d.read_segy(Path(\"./data/velocity.segy.gz\"), header).transpose()\n",
    "vs = segy_mesh_2d.vs_from_poisson(vp)\n",
    "rho = segy_mesh_2d.rho_from_gardeners(vp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can call the SEG-Y mesh helper to generate our mesh. This will happen in 3 stages. First, we'll "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mesh is accurate to this frequency.\n",
    "max_frequency = 10.0\n",
    "\n",
    "# Find the first discontinuity in depth.\n",
    "depths, indices = segy_mesh_2d.find_first_discontinuity(vp, header)\n",
    "\n",
    "# Generate the mesh and interpret the computed depths as the ocean bottom.\n",
    "mesh = segy_mesh_2d.generate_mesh_chunks(vp, max_frequency, header, depths=depths)\n",
    "\n",
    "# Deform the mesh to respect the ocean bottom.\n",
    "mesh = segy_mesh_2d.deform(mesh, depths, header)\n",
    "\n",
    "# Attach the elastic parameters to the mesh.\n",
    "mesh = segy_mesh_2d.attach_parameters(mesh, [\"VP\", \"VS\", \"RHO\"], [vp, vs, rho], header, \n",
    "                                      smoothing_std=100, version=\"V1\", ocean_bottom_depths=depths,\n",
    "                                      ocean_bottom_indices=indices, remove_ocean=False)\n",
    "\n",
    "# Write the mesh and visualize.\n",
    "mesh.write_exodus(\"mesh.e\");mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running simulations with OBS nodes\n",
    "\n",
    "Now we can define an array of receivers that lie directly the ocean bottom. The SEG-Y mesh helper routines have defined a side-set called \"ocean_bottom\" within the mesh file. We can use this side set to compute the spatial locations of OBS nodes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the locations of the receivers by interpolating along the side set.\n",
    "x_loc = np.linspace(mesh.points[:, 0].min(), mesh.points[:, 0].max(), 1000)[100:-100]\n",
    "ss_elem, ss_side = mesh.side_sets[\"ocean_bottom\"]\n",
    "p = mesh.points[mesh.connectivity[ss_elem, ss_side]]\n",
    "y_loc = np.interp(x_loc, p[:, 0].ravel(), p[:, 1].ravel())\n",
    "\n",
    "# Setup receivers.\n",
    "receivers = [{\n",
    "    \"network-code\": \"XX\",\n",
    "    \"station-code\": f\"{_i:03d}\",\n",
    "    \"medium\": \"solid\",\n",
    "    \"location\": [float(x), float(y)]}\n",
    "    for _i, (x, y) in enumerate(zip(x_loc, y_loc - 0.5))]\n",
    "\n",
    "# Setup source.\n",
    "sources = [{\n",
    "    \"scale\": [1e9],\n",
    "    \"name\": \"source1\",\n",
    "    \"location\": [20000.0, 1500.0],\n",
    "    \"spatial_type\": \"scalar\",\n",
    "    \"temporal_type\": \"ricker\",\n",
    "    \"center_frequency\": 5.0    \n",
    "}]\n",
    "\n",
    "with open(\"source.toml\", \"w\") as fh:\n",
    "    toml.dump({\"source\": sources}, fh)\n",
    "\n",
    "# Attach to mesh to get a quick visualization.\n",
    "mesh._meta = {\"receivers\": receivers, \"sources\": sources}\n",
    "mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The rest of this tutorial is rather familiar. We simply define a pressure source in the oceanic domain, and run the simulation with a standard input file. There is, however, one special feature here which is usually not present. Do you see why the \"y1\" sideset is labelled as \"homogeneous-dirichlet\"?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_file = {\n",
    "    # Basic domain information.\n",
    "    \"domain\": {\n",
    "        \"dimension\": 2,\n",
    "        \"polynomial-order\": 4,\n",
    "        \"mesh\": {\n",
    "            \"filename\": \"mesh.e\",\n",
    "            \"format\": \"exodus\"\n",
    "        },\n",
    "        \"model\": {\n",
    "            \"filename\": \"mesh.e\",\n",
    "            \"format\": \"exodus\"\n",
    "        }\n",
    "    },\n",
    "    # Physical parameterization.\n",
    "    \"physics\": {\n",
    "        \"wave-equation\": {\n",
    "            \"time-stepping-scheme\": \"newmark\",\n",
    "            \"start-time-in-seconds\": -0.2,\n",
    "            \"end-time-in-seconds\": 5.0,\n",
    "            \"source-toml-filename\": \"source.toml\",\n",
    "            \"boundaries\": [{\n",
    "                \"type\": \"absorbing\",\n",
    "                \"side-sets\": [\"x0\", \"x1\", \"y0\"]\n",
    "            }, \n",
    "            {\n",
    "                \"type\": \"homogeneous-dirichlet\",\n",
    "                \"side-sets\": [\"y1\"]\n",
    "            }],\n",
    "        }\n",
    "    },\n",
    "    \"output\": {\n",
    "        # Output movie in acoustic and elastic media.\n",
    "        \"volume-data\": {\n",
    "            \"fields\": [\"u_ACOUSTIC\", \"u_ELASTIC\"],\n",
    "            \"sampling-interval-in-time-steps\": 1000,\n",
    "            \"filename\": \"movie.h5\",\n",
    "            \"polynomial-order\": 4,\n",
    "            \"format\": \"HDF5\",\n",
    "            \"region-of-interest\": False\n",
    "        },\n",
    "        # Output strain and displacement on the OBS line.\n",
    "        \"point-data\": {\n",
    "            \"fields\": [\"strain\", \"u_ELASTIC\"],\n",
    "            \"sampling-interval-in-time-steps\": 1,\n",
    "            \"filename\": \"receivers.h5\",\n",
    "            \"format\": \"ASDF\",\n",
    "            \"receiver\": receivers\n",
    "        }\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again below we'll use _Flow_ to submit the job and collect the output. As you may have noticed above in the input file, we've also save acoustic displacement potential and the elastic displacement. So, after the simulation is done, you could go ahead and download the movie if you'd like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run salvus\n",
    "j = salvus_flow.api.run(\n",
    "    ranks=2,\n",
    "    get_all=True,\n",
    "    overwrite=True,\n",
    "    site_name=\"local\",\n",
    "    output_folder=\"output\",\n",
    "    input_file=input_file, \n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can check out a shotgather of our synthetic strain data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_shotgather(asdf_file):\n",
    "    \"\"\"\n",
    "    Generate a shotgather from an ASDF file.\n",
    "    \"\"\"\n",
    "    \n",
    "    strain_map = {\"xx\": 0, \"yy\": 1, \"xy\": 2}\n",
    "    with ASDFDataSet(asdf_file, mode=\"r\") as d:\n",
    "        t = d.waveforms[d.waveforms.list()[0]].strain\n",
    "        arr = np.empty((len(d.waveforms.list()), t[0].stats.npts))\n",
    "        for _i, tr in enumerate(d.waveforms):\n",
    "            arr[_i, :] = tr.strain[strain_map[\"xy\"]].data\n",
    "            \n",
    "    return arr\n",
    "        \n",
    "plt.figure(figsize=(15, 8))\n",
    "gather = make_shotgather(\"./output/receivers.h5\").T\n",
    "plt.imshow(gather, cmap=\"RdBu\", aspect=\"auto\")\n",
    "plt.xlabel(\"Receiver\")\n",
    "plt.ylabel(\"Time sample\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
